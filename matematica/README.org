
* TODO Tareas para armar el Círculo [0/0]
- [X] Selección de temas a trabajar
- [ ] Selección de recursos didácticos
- [ ] Selección de herramientas pedagógicas
- [ ] Montaje en [[https://www.p2pu.org/en/][P2Pu]]
** Temas
Esta selección se arma en función de los [[https://www.educ.ar/recursos/110570/nap-matematica-educacion-secundaria-ciclo-basico][Núcleos de Enseñanza Prioritarios de Secundaria, ciclo básico]]. Paso todos los temas, marco los que creo son interesantes repasar.

- [-] En relación con el número y operaciones
  - [ ] El reconocimiento y uso de los números racionales en situaciones problemáticas que requieran:
    - [ ] interpretar, registrar, comunicar y comparar números enteros en diferentes contextos: como número relativo (temperaturas, nivel del mar) y a partir de la resta de dos naturales (juegos de cartas, pérdidas y ganancias);
    - [ ] comparar números enteros y hallar distancias entre ellos, representándolos en la recta numérica;
    - [ ] interpretar el número racional como cociente;
    - [ ] usar diferentes representaciones de un número racional (expresiones fraccionarias y decimales, notación científica, punto de la recta numérica, etc.), argumentando sobre su equivalencia y eligiendo la representación más adecuada en función del problema a resolver;
    - [ ] analizar diferencias y similitudes entre las propiedades de los números enteros (Z) y los racionales (Q) (orden, discretitud y densidad).
  - [-] El reconocimiento y uso de las operaciones entre números racionales en sus distintas expresiones y la explicitación de sus propiedades en situaciones problemáticas que requieran:
    - [ ] interpretar modelos que den significado a la suma, resta, multiplicación, división y potenciación en Z
    - [X] usar la potenciación (con exponente entero) y la radicación en Q y analizar sus propiedades;
    - [X] analizar las operaciones en Z y sus propiedades como extensión de las elaboradas en N;
    - [X] usar y analizar estrategias de cálculo con números racionales seleccionando el tipo de cálculo (mental y escrito, exacto y aproximado, con y sin uso de la calculadora) y la forma de expresar los números involucrados6 que resulten más convenientes y evaluando la razonabilidad del resultado obtenido;
    - [X] usar la jerarquía y las propiedades de las operaciones en la producción e interpretación de cálculos;
    - [X] explorar y enunciar propiedades ligadas a la divisibilidad en N (suma de dos múltiplos, si un número es múltiplo de otro y este de un tercero, el primero es múltiplo del tercero, etc.).
- [ ] En relación con el álgebra y las funciones
  - [ ] El uso de relaciones entre variables en situaciones problemáticas que requieran:
    - [ ] interpretar relaciones entre variables en tablas, gráficos y fórmulas en diversos contextos (regularidades numéricas, proporcionalidad directa e inversa, etc.);
    - [ ] modelizar variaciones uniformes y expresarlas eligiendo la representación más adecuada a la situación;
    - [ ] explicitar y analizar propiedades de las funciones de proporcionalidad directa (variación uniforme, origen en el cero);
    - [ ] producir y comparar fórmulas para analizar las variaciones de perímetros, áreas y volúmenes, en función de la variación de diferentes dimensiones de figuras y cuerpos;
    - [ ] producir fórmulas para representar regularidades numéricas en N y analizar sus equivalencias.
  - [ ] El uso de ecuaciones y otras expresiones algebraicas en situaciones problemáticas que requieran:
    - [ ] producir y analizar afirmaciones sobre propiedades de las operaciones o criterios de divisibilidad avanzando desde su expresión oral a su expresión simbólica, y argumentar sobre su validez;
    - [ ] transformar expresiones algebraicas obteniendo expresiones equivalentes que permitan reconocer relaciones no identificadas fácilmente en la expresión original, usando diferentes propiedades al resolver ecuaciones del tipo $ax + b = cx + d$
    - [ ] usar ecuaciones lineales con una variable como expresión de una condición sobre un conjunto de números y analizar su conjunto solución (solución única, infinitas soluciones, sin solución).
* Recursos didácticos
** Matemática para aprender más Ciclo Básico
Doc :: [[contenidos/pnam.secundaria.estudiantes.web_.pdf]]
*** Predecir
- interpretar, registrar, comunicar y comparar números enteros en diferentes contextos: como número relativo (temperaturas, nivel del mar) y a partir de la resta de dos naturales (juegos de cartas, pérdidas y ganancias);
- interpretar relaciones entre variables en tablas, gráficos y fórmulas en diversos contextos (regularidades numéricas, proporcionalidad directa e inversa, etc.);
** Números y operaciones
- ¿Cómo cambia mi altura conforme pasan los años?
